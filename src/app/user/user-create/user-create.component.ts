import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Store, select, ReducerManagerDispatcher, ActionsSubject } from '@ngrx/store';
import { CreateUser, UserActionTypes, EditUserSuccess } from 'src/app/store/user/user.actions';
import { UserInterface } from 'src/app/shared/user.interface';
import { selectUser } from 'src/app/store/user/user.selectors';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss'],
})
export class UserCreateComponent implements OnInit, OnDestroy {
  form: FormGroup;
  userSelected = false;
  private destroy$ = new Subject();

  constructor(
    private fb: FormBuilder,
    private store: Store<{}>,
  ) {}

  ngOnInit(): void {
    this.store
    .pipe(select(selectUser),
    takeUntil(this.destroy$),
    )
    .subscribe((user) => {
      if (user) {
        this.userSelected = true;
        this.form.controls.username.setValue(user.username);
        this.form.controls.firstName.setValue(user.firstName);
        this.form.controls.lastName.setValue(user.lastName);
        this.form.controls.role.setValue(user.role);
        this.form.controls.enabled.setValue(user.enabled);
      }
    });
    this.form = this.fb.group({
      username: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      role: [''],
      enabled: [false],
    });
  }

  submitForm(): void {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      if (!this.userSelected) {
        if (this.form.controls.role.value === '') {
          this.form.controls.role.setValue('user');
        }
        this.store.dispatch(new CreateUser({ user: this.form.value }));
      } else {
        this.store.dispatch(new EditUserSuccess({ user: this.form.value }));
        this.userSelected = false;
      }
      this.form.reset();
      }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
