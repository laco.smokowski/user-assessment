import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import {
  selectUserList,
  selectPageState,
} from 'src/app/store/user/user.selectors';
import { pageState } from '../../shared/page-state.interface';
import { Observable, Subject } from 'rxjs';
import {
  ChangeUserListPage,
  FilterUserList,
  EditUser,
  DeleteUser,
} from 'src/app/store/user/user.actions';
import { FormControl } from '@angular/forms';
import { debounceTime, takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit, OnDestroy {
  userList$;
  pageState$: Observable<pageState>;
  filterInput = new FormControl('');
  roleFilterSelect = new FormControl('');
  query = ['', ''];
  private destroy$ = new Subject();

  constructor(private store: Store<{}>) {}

  ngOnInit(): void {
    this.userList$ = this.store.pipe(select(selectUserList));
    this.pageState$ = this.store.pipe(select(selectPageState));
    this.filterInput.valueChanges
      .pipe(debounceTime(400), takeUntil(this.destroy$))
      .subscribe((query) => {
        this.query[1] = query;
        this.store.dispatch(new FilterUserList({ query: [this.query[0], query] }));
      }

      );
  }

  filterByRole(e): void {
    this.query[0] = e.target.value;
    this.store.dispatch(new FilterUserList({ query: [e.target.value, this.query[1]] }));
  }

  changePage(i): void {
    this.store.dispatch(new ChangeUserListPage({ page: i }));
  }

  editUser(userListId): void {
    this.store.dispatch(new EditUser({ userListId }));
  }

  deleteUser(userListId): void {
    console.log(userListId);
    this.store.dispatch(new DeleteUser({ userListId }));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
