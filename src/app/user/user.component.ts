import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { CreateUser } from '../store/user/user.actions';
import { UserInterface } from '../shared/user.interface';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  constructor(private store: Store<{}>) {}

  ngOnInit(): void {
    this.createBatchData();
  }

  createBatchData(): void {
    for (let i = 0; i < 23; i++) {
      const user: UserInterface = {
        username: `username ${String.fromCharCode(97 + i)} ${i}`,
        firstName: `name ${String.fromCharCode(98 + i)} ${i}`,
        lastName: `last ${i}`,
        role: Math.random() >= 0.9 ? 'admin' : 'user',
        enabled: Math.random() >= 0.5,
      };
      this.store.dispatch(new CreateUser({ user }));
    }
  }
}
