export interface UserInterface {
  username: string;
  firstName: string;
  lastName: string;
  role: 'admin' | 'user';
  enabled: boolean;
}
