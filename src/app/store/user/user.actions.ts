import { UserInterface } from '../../shared/user.interface';
import { Action } from '@ngrx/store';
import { UserListComponent } from 'src/app/user/user-list/user-list.component';

export enum UserActionTypes {
  CreateUser = '[User] Create New User',
  DeleteUser = '[User] Delete User',
  EditUser = '[User] Edit User',
  EditUserSuccess = '[User] Edit User Success',
  GetUserList = '[Users] Get User List',
  FilterUserList = '[Users] Filter User List',
  FilterUserSuccess = '[Users] Filter User List Success',
  ChangeUserListPage = '[User List] Change User List Page',
}

export class ChangeUserListPage implements Action {
  readonly type = UserActionTypes.ChangeUserListPage;
  constructor(public payload: { page: number }) {}
}

export class DeleteUser implements Action {
  readonly type = UserActionTypes.DeleteUser;
  constructor(public payload: { userListId: number }) {}
}

export class EditUser implements Action {
  readonly type = UserActionTypes.EditUser;
  constructor(public payload: { userListId: number }) {}
}

export class EditUserSuccess implements Action {
  readonly type = UserActionTypes.EditUserSuccess;
  constructor(public payload: { user: UserInterface }) {}
}

export class CreateUser implements Action {
  readonly type = UserActionTypes.CreateUser;
  constructor(public payload: { user: UserInterface }) {}
}

export class GetUserList implements Action {
  readonly type = UserActionTypes.GetUserList;
}

export class FilterUserList implements Action {
  readonly type = UserActionTypes.FilterUserList;
  constructor(public payload: { query: string[] }) {}
}

export class FilterUserListSuccess implements Action {
  readonly type = UserActionTypes.FilterUserSuccess;
  constructor(public payload: { userList: UserInterface[] }) {}
}

export type UserAction =
  | CreateUser
  | GetUserList
  | FilterUserList
  | FilterUserListSuccess
  | ChangeUserListPage
  | DeleteUser
  | EditUser
  | EditUserSuccess;
