import { createSelector } from '@ngrx/store';
import { UserState } from './user.reducer';

export interface AppState {
  users: UserState;
}

export const selectUsers = (state: AppState) => {
  return state.users;
};

export const selectUserList = createSelector(
  selectUsers,
  (state: UserState) => {
    return state.usersOnPage;
  }
);

export const selectUser = createSelector(
  selectUsers,
  (state: UserState) => {
    return state.userList[state.selectedUserId];
  }
)

export const selectPageState = createSelector(
  selectUsers,
  (state: UserState) => {
    const pageNumber = Math.ceil(state.userList.length / state.usersPerPage);
    return {
      page: state.page,
      pages: Array.from(Array(pageNumber), (_, i) => i + 1),
    };
  }
);
