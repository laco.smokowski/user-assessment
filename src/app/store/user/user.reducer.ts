import { UserInterface } from '../../shared/user.interface';
import { UserAction, UserActionTypes } from './user.actions';

export interface UserState {
  loading: boolean;
  query: string[];
  allUsers: UserInterface[];
  usersOnPage: UserInterface[];
  userList: UserInterface[];
  page: number;
  usersPerPage: number;
  selectedUserId: number;
}

export const initialState = {
  loading: false,
  query: ['', ''],
  allUsers: [],
  usersOnPage: [],
  userList: [],
  page: 1,
  usersPerPage: 5,
  selectedUserId: -1,
};

export function getUsersPerPage (state: UserState): UserInterface[] {
  const startIndex = (state.page - 1) * state.usersPerPage;
  const endIndex = Math.min(
    startIndex + state.usersPerPage,
    state.userList.length
  );
  return state.userList.slice(startIndex, endIndex);
}

export function UserReducer(
  state = initialState,
  action: UserAction
): UserState {
  switch (action.type) {
    case UserActionTypes.CreateUser: {
      const userList = [...state.allUsers, action.payload.user];
      return {
        ...state,
        allUsers: userList,
        userList,
        usersOnPage: getUsersPerPage({ ...state, userList }),
      };
    }
    case UserActionTypes.FilterUserList: {
      const userList = state.allUsers.filter(
          (user) =>
            (user.firstName.includes(action.payload.query[1]) ||
            user.lastName.includes(action.payload.query[1]) ||
            user.username.includes(action.payload.query[1])) &&
            user.role.includes(action.payload.query[0])
        );
      return {
        ...state,
        userList,
        usersOnPage: getUsersPerPage({ ...state, userList }),
      };
    }
    case UserActionTypes.DeleteUser: {
      const userList = [
        ...state.userList.slice(0, action.payload.userListId),
        ...state.userList.slice(action.payload.userListId + 1),
      ];
      const allUsers = state.allUsers.filter(
        (user) => user !== state.userList[action.payload.userListId]
      );
      return {
        ...state,
        userList,
        allUsers,
        usersOnPage: getUsersPerPage({ ...state, userList }),
      };
    }
    case UserActionTypes.EditUser: {
      return {
        ...state,
        selectedUserId: action.payload.userListId,
      };
    }

    case UserActionTypes.EditUserSuccess: {
      const allUsers = [...state.allUsers];
      allUsers[
        allUsers.findIndex(
          (user) => user === state.userList[state.selectedUserId]
        )
      ] = action.payload.user;
      const userList = [...state.userList];
      userList[state.selectedUserId] = action.payload.user;
      return {
        ...state,
        allUsers,
        userList,
        usersOnPage: getUsersPerPage({ ...state, userList }),
      };
    }
    case UserActionTypes.FilterUserSuccess: {
      return {
        ...state,
        userList: action.payload.userList,
      };
    }
    case UserActionTypes.GetUserList: {
      const userList = [...state.allUsers];
      return {
        ...state,
        userList: getUsersPerPage({ ...state, userList }),
      };
    }
    case UserActionTypes.ChangeUserListPage: {
      const page = action.payload.page;
      return {
        ...state,
        page,
        usersOnPage: getUsersPerPage({ ...state, page }),
      };
    }
  }
  return state;
}
